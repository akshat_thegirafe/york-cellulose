//STELLERNAV JS STARTS  
  jQuery('#main-nav1').stellarNav({
    theme     : 'plain', 
    breakpoint: 1199, 
    mobileMode: false,
    phoneBtn: false,
    locationBtn: false, 
    sticky     : false, 
    openingSpeed: 250, 
    closingDelay: 250, 
    position: 'right', 
     menuLabel: '',
    showArrows: true, 
    closeBtn     : false, 
    scrollbarFix: false
  });
//STELLERNAV JS ENDS


// STICKY JS STARTS
     $(window).on('scroll', function() {
    if ($(window).scrollTop() > 200) {
      $('#navbar').addClass('sticky');
    } else {
      $('#navbar').removeClass('sticky');
    }
  });

// STICKY JS ENDS



// HEADER FOOTER CALL FUNCTION STARTS
  $(function(){
    $("#header").load("header.html");
    $("#footer").load("footer.html");
  });
  $(function(){
    $("#header2").load("header2.html");
  });
// HEADER FOOTER CALL FUNCTION ENDS



// SCROLL TO TOP JS STARTS
  $(document).ready(function(){ 
    $(window).scroll(function(){ 
      if ($(this).scrollTop() > 100) { 
        $('#scroll').fadeIn(); 
      } 
      else{ 
        $('#scroll').fadeOut(); 
      } 
    }); 
    $('#scroll').click(function(){ 
    $("html, body").animate({ scrollTop: 0 }, 600); 
        return false; 
    });   
  });
// SCROLL TO TOP JS ENDS





// SEARCH JS STARTS
    
    $('#searchIcon').on('click', function () {
        $('.search-form').toggleClass('active');
    });
    $('.closeIcon').on('click', function () {
        $('.search-form').removeClass('active');
    });

// SEARCH JS ENDS

$('#banner').owlCarousel({
    // autoplay: true,
    smartSpeed: 900,
    loop: true,
    margin: 20,
    nav: false,
    center:false,
    autoplay:true,
    navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
    dots: false,
    responsive:{
        0:{
            items:1,
            nav: false
        },
        575:{
            items:1,
            nav: false
        },
        768:{
            items:1,
            nav: false
        },
        992:{
            items:1
        },
        1200:{
            items:1
        }
    }
});


// SLIDER 1 JS STARTS
    $('#slider-resi').owlCarousel({
        // autoplay: true,
        smartSpeed: 900,
        loop: true,
        margin: 20,
        nav: true,
        center:true,
        navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
        dots: false,
        responsive:{
            0:{
                items:2,
                nav: false
            },
            575:{
                items:2,
                nav: false
            },
            768:{
                items:3,
                nav: false
            },
            992:{
                items:3
            },
            1200:{
                items:4
            }
        }
    });

// SLIDER 1 JS ENDS

// SLIDER 1 JS STARTS
    $('#slider-brands').owlCarousel({
        autoplay: true,
        smartSpeed: 900,
        loop: true,
        margin: 30,
        nav: true,
        navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
        dots: false,
        responsive:{
            0:{
                items:3,
                nav: false
            },
            575:{
                items:3,
                nav: false
            },
            768:{
                items:3,
                nav: false
            },
            992:{
                items:4
            },
            1200:{
                items:5
            }
        }
    });

// SLIDER 1 JS ENDS


$('#also-viewed').owlCarousel({
    // autoplay: true,
    smartSpeed: 900,
    loop: true,
    margin: 20,
    nav: false,
    center:false,
    autoplay:true,
    navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
    dots: false,
    responsive:{
        0:{
            items:2,
            nav: false
        },
        575:{
            items:2,
            nav: false
        },
        768:{
            items:3,
            nav: false
        },
        992:{
            items:6
        },
        1200:{
            items:6
        }
    }
});
$('#related-prod').owlCarousel({
    // autoplay: true,
    smartSpeed: 900,
    loop: true,
    margin: 20,
    nav: false,
    center:false,
    autoplay:true,
    navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
    dots: false,
    responsive:{
        0:{
            items:2,
            nav: false
        },
        575:{
            items:2,
            nav: false
        },
        768:{
            items:3,
            nav: false
        },
        992:{
            items:6
        },
        1200:{
            items:6
        }
    }
});

$('#more-items').owlCarousel({
    // autoplay: true,
    smartSpeed: 900,
    loop: true,
    margin: 20,
    nav: false,
    center:false,
    autoplay:true,
    navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
    dots: false,
    responsive:{
        0:{
            items:2,
            nav: false
        },
        575:{
            items:2,
            nav: false
        },
        768:{
            items:3,
            nav: false
        },
        992:{
            items:6
        },
        1200:{
            items:6
        }
    }
});


$('#featured').owlCarousel({
    // autoplay: true,
    smartSpeed: 900,
    loop: true,
    margin: 20,
    nav: false,
    center:false,
    autoplay:true,
    navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
    dots: false,
    responsive:{
        0:{
            items:2,
            nav: false
        },
        575:{
            items:2,
            nav: false
        },
        768:{
            items:3,
            nav: false
        },
        992:{
            items:6
        },
        1200:{
            items:6
        }
    }
});


$('#wish1').owlCarousel({
    // autoplay: true,
    smartSpeed: 900,
    loop: true,
    margin: 20,
    nav: false,
    center:false,
    autoplay:true,
    navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
    dots: false,
    responsive:{
        0:{
            items:2,
            nav: false
        },
        575:{
            items:2,
            nav: false
        },
        768:{
            items:3,
            nav: false
        },
        992:{
            items:6
        },
        1200:{
            items:6
        }
    }
});

$('#wish2').owlCarousel({
    // autoplay: true,
    smartSpeed: 900,
    loop: true,
    margin: 20,
    nav: false,
    center:false,
    autoplay:true,
    navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
    dots: false,
    responsive:{
        0:{
            items:2,
            nav: false
        },
        575:{
            items:2,
            nav: false
        },
        768:{
            items:3,
            nav: false
        },
        992:{
            items:6
        },
        1200:{
            items:6
        }
    }
});

$('#detail1').owlCarousel({
    // autoplay: true,
    smartSpeed: 900,
    loop: true,
    margin: 20,
    nav: false,
    center:false,
    autoplay:true,
    navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
    dots: false,
    responsive:{
        0:{
            items:2,
            nav: false
        },
        575:{
            items:2,
            nav: false
        },
        768:{
            items:3,
            nav: false
        },
        992:{
            items:6
        },
        1200:{
            items:6
        }
    }
});

$('#detail2').owlCarousel({
    // autoplay: true,
    smartSpeed: 900,
    loop: true,
    margin: 20,
    nav: false,
    center:false,
    autoplay:true,
    navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
    dots: false,
    responsive:{
        0:{
            items:2,
            nav: false
        },
        575:{
            items:2,
            nav: false
        },
        768:{
            items:3,
            nav: false
        },
        992:{
            items:6
        },
        1200:{
            items:6
        }
    }
});


$('#track1').owlCarousel({
    // autoplay: true,
    smartSpeed: 900,
    loop: true,
    margin: 20,
    nav: false,
    center:false,
    autoplay:true,
    navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
    dots: false,
    responsive:{
        0:{
            items:2,
            nav: false
        },
        575:{
            items:2,
            nav: false
        },
        768:{
            items:3,
            nav: false
        },
        992:{
            items:6
        },
        1200:{
            items:6
        }
    }
});

$('#track2').owlCarousel({
    // autoplay: true,
    smartSpeed: 900,
    loop: true,
    margin: 20,
    nav: false,
    center:false,
    autoplay:true,
    navText: ['<img src="images/prev.png">','<img src="images/next.png">'],
    dots: false,
    responsive:{
        0:{
            items:2,
            nav: false
        },
        575:{
            items:2,
            nav: false
        },
        768:{
            items:3,
            nav: false
        },
        992:{
            items:6
        },
        1200:{
            items:6
        }
    }
});


//VALIDATION STARTS
    
    function ValidateAlpha(evt)
    {
        var keyCode = (evt.which) ? evt.which : evt.keyCode
        if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)
         
        return false;
            return true;
    }

    function ValidateEmail() {
        var email = document.getElementById("txtEmail").value;
        var lblError = document.getElementById("lblError");
        lblError.innerHTML = "";
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (!expr.test(email)) {
            lblError.innerHTML = "Invalid email address.";
        }
    }



    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }


     $(function(){
 
    $('#message').keyup(function()
    {
        var yourInput = $(this).val();
        re = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi;
        var isSplChar = re.test(yourInput);
        if(isSplChar)
        {
            var no_spl_char = yourInput.replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '');
            $(this).val(no_spl_char);
        }
    });
 
});
//VALIDATIONS ENDS



// ENCRYPTION STARTS
  // document.onkeydown = function(e) {
  //   if(e.keyCode == 123) {
  //     return false;
  //   }
  //   if(e.ctrlKey && e.shiftKey && e.keyCode == 'I'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.shiftKey && e.keyCode == 'J'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.shiftKey && e.keyCode == 'M'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.keyCode == 'U'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.keyCode == 'C'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.keyCode == 'X'.charCodeAt(0)){
  //     return false;
  //   }
  //   if(e.ctrlKey && e.shiftKey && e.keyCode == 'C'.charCodeAt(0)){
  //     return false;
  //   }      
  // }

  // $(document).ready(function() {
  //   $("body").on("contextmenu",function(){
  //     return false;
  //   }); 
  // });

  // document.addEventListener('contextmenu', event => event.preventDefault());
  // $("img").mousedown(function(){
  //   return false;
  // });
// ENCRYPTION ENDS